const fs = require('fs');

exports.incrementer = function incrementer(delta){
    return function (n){return n + delta;};
}

exports.rms = function rms(){
    let args = Array.from(arguments);
    return Math.sqrt(args
        .map(n => n*n)
        .reduce((sum,each) => sum + each, 0) 
        / args.length
    );
}

function condenseMklog(warnings, outfile){
    let warnings_dict = warnings.reduce((accum_obj, warning) => {
        let fname = warning[0];
        let lineno = warning[1];
        let errmsg = warning[2];
        if (!accum_obj[fname]){
            accum_obj[fname] = {};
        }
        if (!accum_obj[fname][lineno]){
            accum_obj[fname][lineno] = [];
        }
        accum_obj[fname][lineno].push(errmsg);
        return accum_obj;
    }, {})

    fs.writeFile(outfile, JSON.stringify(warnings_dict), function(err){
        if (err) console.log("Could not write file.");
    })
}

exports.processMklog = function processMklog(mklog_name, outfile){
    let warning_expr = /(.+):(\d+): warning:(.+)/g;
    fs.readFile(mklog_name, 'utf-8', function(err, data){
        let warnings = [];
        if(err){
            console.log("Couldn't read input file");
        } else{
            while(match = warning_expr.exec(data)){
                warnings.push(match.slice(1,4));
            }
            condenseMklog(warnings,outfile);
        }
    })
}